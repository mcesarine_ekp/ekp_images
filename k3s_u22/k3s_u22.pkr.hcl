packer {
  required_plugins {
    qemu = {
      source  = "github.com/hashicorp/qemu"
      version = "~> 1"
    }
    vagrant = {
      source  = "github.com/hashicorp/vagrant"
      version = "~> 1"
    }
  }
}

locals {
  ubuntu_version  = "jammy"
  k3s_version     = "1.30.1+k3s1"
  calico_version  = "3.28.0"
  metallb_version = "0.14.5"
  helm_version    = "3.16.2"
  vm_name         = "ekp-k3s-u22"
  output_base     = "${path.root}/../output/k3s_u22"
  output = {
    kvm_k3s_u22     = "${local.output_base}/kvm"
    vagrant_k3s_u22 = "${local.output_base}/vagrant"
  }
}

source "qemu" "kvm_k3s_u22" {
  accelerator      = "kvm"
  cd_files         = ["./cloud-init/*"]
  cd_label         = "cidata"
  disk_compression = true
  disk_image       = true
  disk_size        = "10G"
  headless         = true
  iso_checksum     = "file:https://cloud-images.ubuntu.com/${local.ubuntu_version}/current/SHA256SUMS"
  iso_url          = "https://cloud-images.ubuntu.com/${local.ubuntu_version}/current/${local.ubuntu_version}-server-cloudimg-amd64-disk-kvm.img"
  output_directory = local.output.kvm_k3s_u22
  shutdown_command = "echo 'packer' | sudo -S shutdown -P now"
  ssh_password     = "ubuntu"
  ssh_username     = "ubuntu"
  vm_name          = "${local.vm_name}.img"
  qemuargs = [
    ["-m", "2048M"],
    ["-smp", "2"],
    ["-serial", "mon:stdio"],
  ]
}

source "vagrant" "vagrant_k3s_u22" {
  provider     = "virtualbox"
  source_path  = "https://cloud-images.ubuntu.com/${local.ubuntu_version}/current/${local.ubuntu_version}-server-cloudimg-amd64-vagrant.box"
  checksum     = "file:https://cloud-images.ubuntu.com/${local.ubuntu_version}/current/SHA256SUMS"
  output_dir   = local.output.vagrant_k3s_u22
  communicator = "ssh"
}

build {
  sources = [
    "source.qemu.kvm_k3s_u22",
    "source.vagrant.vagrant_k3s_u22",
  ]

  provisioner "file" {
    source      = "scripts/cloud-init/cc_ekp.py"
    destination = "/tmp/cc_ekp.py"
  }

  provisioner "file" {
    source      = "scripts/cloud-init/cloud.cfg"
    destination = "/tmp/cloud.cfg"
  }

  provisioner "shell" {
    // run scripts with sudo, as the default cloud image user is unprivileged
    execute_command = "echo 'packer' | sudo -S sh -c '{{ .Vars }} {{ .Path }}'"
    env = {
      K3S_VERSION     = local.k3s_version
      CALICO_VERSION  = local.calico_version
      METALLB_VERSION = local.metallb_version
      HELM_VERSION    = local.helm_version
    }
    // NOTE: cleanup.sh should always be run last, as this performs post-install cleanup tasks
    scripts = [
      "scripts/install.sh",
      "scripts/ekp-cloud-init.sh",
      "scripts/cleanup.sh",
    ]
  }

  provisioner "shell" {
    name = "Vagrant_specific"
    // run scripts with sudo, as the default cloud image user is unprivileged
    execute_command = "echo 'packer' | sudo -S sh -c '{{ .Vars }} {{ .Path }}'"
    scripts = [
      "scripts/vagrant.sh",
    ]
    only = ["vagrant.vagrant_k3s_u22"]
  }

  post-processor "checksum" {
    checksum_types = ["sha1", "sha256", "sha512"]
    output         = "${local.output[source.name]}/${local.vm_name}_{{.ChecksumType}}.checksum"
  }

  post-processor "shell-local" {
    name = "Rename_vagrant_box"
    inline = [
      "rm -Rf ${local.output[source.name]}/.vagrant ${local.output[source.name]}/Vagrantfile",
      "mv ${local.output[source.name]}/package.box ${local.output[source.name]}/${local.vm_name}.box",
    ]
    only = ["vagrant.vagrant_k3s_u22"]
  }

  post-processor "shell-local" {
    name = "Install_vagrant_box_locally"
    inline = [
      "vagrant box add --force --provider virtualbox --name ${local.vm_name} ${local.output[source.name]}/${local.vm_name}.box",
    ]
    only = ["vagrant.vagrant_k3s_u22"]
  }
}
