#!/usr/bin/env bash

echo "Install | Update apt"
apt-get update

echo "Install | Install requirements"
apt-get install -y qemu-guest-agent nfs-common python3-yaml

echo "Install | Install k3s installer"
wget -O /usr/local/bin/install_k3s https://get.k3s.io
chmod +x /usr/local/bin/install_k3s

echo "Install | Install k3s binary"
wget -O /usr/local/bin/k3s https://github.com/k3s-io/k3s/releases/download/v${K3S_VERSION}/k3s
chmod +x /usr/local/bin/k3s

echo "Install | Create ekp lib folder"
mkdir -p /usr/local/lib/ekp

echo "Install | Copy calico definitions"
wget -O /usr/local/lib/ekp/calico_operator.yml https://raw.githubusercontent.com/projectcalico/calico/v${CALICO_VERSION}/manifests/tigera-operator.yaml
wget -O /usr/local/lib/ekp/calico_crd.yml https://raw.githubusercontent.com/projectcalico/calico/v${CALICO_VERSION}/manifests/custom-resources.yaml

echo "Install | Copy metallb definitions"
wget -O /usr/local/lib/ekp/metallb.yml https://raw.githubusercontent.com/metallb/metallb/v${METALLB_VERSION}/config/manifests/metallb-native.yaml

echo "Install | Install helm CLI"
wget https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz
tar -xf helm-v${HELM_VERSION}-linux-amd64.tar.gz
mv linux-amd64/helm /usr/local/bin/helm
chmod +x /usr/local/bin/helm
