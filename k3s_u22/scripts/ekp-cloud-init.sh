#!/usr/bin/env bash

echo "ekp | Install cloud-init module"
mv /tmp/cc_ekp.py /usr/lib/python3/dist-packages/cloudinit/config/cc_ekp.py
chown root.root /usr/lib/python3/dist-packages/cloudinit/config/cc_ekp.py

echo "ekp | Enabling cloud-init module"
mv /etc/cloud/cloud.cfg /etc/cloud/cloud.cfg.ori
mv /tmp/cloud.cfg /etc/cloud/cloud.cfg
chown root.root /etc/cloud/cloud.cfg
