# This file is part of cloud-init. See LICENSE file for license information.
"""ekp Module: Will initialize ekp node/cluster"""

import logging
import random
import string
import subprocess
import tempfile
import time
import yaml
from pathlib import Path
from cloudinit.cloud import Cloud
from cloudinit.config import Config
from cloudinit.config.schema import MetaSchema, get_meta_doc
from cloudinit.distros import ALL_DISTROS
from cloudinit.settings import PER_INSTANCE

MODULE_DESCRIPTION = """\
Description that will be used in module documentation.

This will likely take multiple lines.
"""

LOG = logging.getLogger(__name__)

meta: MetaSchema = {
    "id": "cc_ekp",
    "name": "ekp module",
    "title": "Will initialize ekp node/cluster",
    "description": MODULE_DESCRIPTION,
    "distros": [ALL_DISTROS],
    "frequency": PER_INSTANCE,
    "activate_by_schema_keys": ["ekp"],
    "examples": [
        "example_key: example_value",
        "example_other_key: ['value', 2]",
    ],
}

__doc__ = get_meta_doc(meta)


class CommandException(Exception):
    pass


def launch_helm_command(
    command: list[str], env: dict[str] = {}, max_retries: int = 0, wait=False
) -> None:
    HELM_BIN = "/usr/local/bin/helm"
    env["KUBECONFIG"] = "/etc/rancher/k3s/k3s.yaml"
    launch_command([HELM_BIN] + command, env=env, max_retries=max_retries, wait=wait)


def launch_kubectl_command(
    command: list[str], env: dict[str] = {}, max_retries: int = 0, wait=False
) -> None:
    KUBECTL_BIN = "/usr/local/bin/kubectl"
    env["KUBECONFIG"] = "/etc/rancher/k3s/k3s.yaml"
    launch_command([KUBECTL_BIN] + command, env=env, max_retries=max_retries, wait=wait)


def launch_command(
    command: list[str], env: dict[str] = {}, max_retries: int = 0, wait=False
) -> None:
    last_returncode = 1
    process = None
    retry_counter = 0

    while last_returncode != 0 or retry_counter <= max_retries:
        process = subprocess.run(
            command,
            shell=False,
            env=env,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
        )
        last_returncode = process.returncode
        retry_counter += 1
        if last_returncode != 0:
            if not wait:
                LOG.debug(process.stdout)
                LOG.debug("Error... Retrying...")
            time.sleep(10)

    if last_returncode != 0:
        raise CommandException(
            f"Got issue launching command\n{' '.join(final_command)}\n{process.stdout}"
        )


def handle(name: str, cfg: Config, cloud: Cloud, args: list) -> None:
    LOG.debug(f"BEGIN module {name}")

    if "version" not in cfg["ekp"].keys():
        raise Exception("Missing `version` key in provided config")

    if "mode" not in cfg["ekp"].keys():
        raise Exception("Missing `mode` key in provided config")

    cluster_init = False
    if cfg["ekp"]["mode"] == "server" and "master_url" not in cfg["ekp"].keys():
        cluster_init = True

    # Init command
    init_env_vars = {
        "INSTALL_K3S_VERSION": str(cfg["ekp"]["version"]),
        "K3S_KUBECONFIG_MODE": "644",
        "INSTALL_K3S_EXEC": cfg["ekp"]["mode"],
        "INSTALL_K3S_SKIP_DOWNLOAD": "true",
    }
    init_cmd = [
        "/usr/local/bin/install_k3s",
        "--flannel-backend=none",
        "--disable-network-policy",
        "--disable=servicelb",
    ]
    if "token" in cfg["ekp"].keys():
        init_cmd.extend(
            [
                "--token",
                cfg["ekp"]["token"],
            ]
        )

    if "master_url" in cfg["ekp"].keys():
        init_cmd.extend(
            [
                "--server",
                cfg["ekp"]["master_url"],
            ]
        )

    if "labels" in cfg["ekp"].keys():
        for label in cfg["ekp"]["labels"]:
            init_cmd.extend(
                [
                    "--node-label",
                    label,
                ]
            )

    if "node_ip" in cfg["ekp"].keys():
        init_cmd.extend(
            [
                "--node-ip",
                cfg["ekp"]["node_ip"],
            ]
        )

    if "cluster_cidr" in cfg["ekp"].keys():
        init_cmd.extend(
            [
                "--cluster-cidr",
                cfg["ekp"]["cluster_cidr"],
            ]
        )

    if "service_cidr" in cfg["ekp"].keys():
        init_cmd.extend(
            [
                "--service-cidr",
                cfg["ekp"]["service_cidr"],
            ]
        )

    if cluster_init:
        init_cmd.append("--cluster-init")

    LOG.debug(f"env variables: {str(init_env_vars)}")
    LOG.debug(f"init command: {' '.join(init_cmd)}")
    command = subprocess.run(
        init_cmd,
        env=init_env_vars,
        shell=False,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
    )

    LOG.debug(f"Return code {command.returncode}")
    LOG.debug(f"Output {command.stdout}")

    KUBECTL_BIN = "/usr/local/bin/kubectl"

    if cluster_init:
        retry = 5
        delay = 2
        counter = 0
        started = False
        test_cmd = [KUBECTL_BIN, "get", "nodes"]

        # Waiting for cluster to start
        while not started and counter <= retry:
            command = subprocess.run(
                test_cmd,
                shell=False,
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
            )
            time.sleep(2)
            started = command.returncode == 0
            counter += 1

        if not started:
            raise Exception(f"Got issue waiting for k3s startup\n{command.stdout}")

        LOG.debug("k3s is successfully started")

        # Installing calico operator
        LOG.debug("Beginning calico operator installation")
        calico_operator_cmd = [
            "create",
            "-f",
            "/usr/local/lib/ekp/calico_operator.yml",
        ]
        launch_kubectl_command(calico_operator_cmd)

        # Wait for calico operator to be ready
        LOG.debug("Wait for calico operator readiness")
        calico_operator_wait_cmd = [
            "wait",
            "--for=condition=available",
            "deployment/tigera-operator",
            "-n",
            "tigera-operator",
            "--timeout=2m",
        ]
        launch_kubectl_command(calico_operator_wait_cmd, wait=True)

        LOG.debug("calico operator installed")

        # Installing calico CRDs
        LOG.debug("Beginning calico CRDs installation")
        calico_crd_cmd = [
            "create",
            "-f",
            "/usr/local/lib/ekp/calico_crd.yml",
        ]
        launch_kubectl_command(calico_crd_cmd)

        # Wait for calico deployments namespace to be created
        LOG.debug("Wait for calico CRDs readiness")
        calico_wait_cmd = [
            "get",
            "namespace/calico-apiserver",
        ]
        launch_kubectl_command(calico_wait_cmd, max_retries=12, wait=True)

        # Wait for calico deployments to be ready
        calico_wait_cmd = [
            "wait",
            "--for=condition=available",
            "deployment/calico-apiserver",
            "-n",
            "calico-apiserver",
            "--timeout=2m",
        ]
        launch_kubectl_command(calico_wait_cmd, wait=True)

        LOG.debug("calico CRDs installed")

        # Installing metallb operators
        LOG.debug("Beginning metallb operator installation")
        metallb_cmd = [
            "create",
            "-f",
            "/usr/local/lib/ekp/metallb.yml",
        ]
        launch_kubectl_command(metallb_cmd)

        LOG.debug("Wait for metallb operator readiness")
        metallb_wait_cmd = [
            "wait",
            "--for=condition=available",
            "deployment/controller",
            "-n",
            "metallb-system",
            "--timeout=2m",
        ]
        launch_kubectl_command(metallb_wait_cmd, wait=True)

        LOG.debug("metallb installed")

        if "management_ip" in cfg["ekp"].keys():
            LOG.debug("Installing management IP pool")
            metallbDefinitions = [
                {
                    "apiVersion": "metallb.io/v1beta1",
                    "kind": "IPAddressPool",
                    "metadata": {
                        "name": "ekpmgmt01",
                        "namespace": "metallb-system",
                    },
                    "spec": {
                        "addresses": [f"{cfg['ekp']['management_ip']}/32"],
                        "autoAssign": False,
                    },
                },
                {
                    "apiVersion": "metallb.io/v1beta1",
                    "kind": "L2Advertisement",
                    "metadata": {
                        "name": "ekpmgmt01",
                        "namespace": "metallb-system",
                    },
                    "spec": {
                        "ipAddressPools": [
                            "ekpmgmt01",
                        ],
                    },
                },
            ]
            file = tempfile.NamedTemporaryFile(mode="w+", delete=False)
            yaml.dump_all(metallbDefinitions, file)
            file.close()

            metallb_cmd = ["create", "-f", file.name]
            try:
                launch_kubectl_command(metallb_cmd)
            except ex as CommandException:
                Path(file.name).unlink()
                raise ex

            LOG.debug("Management IP pool installed")

            LOG.debug("Update kubernetes service to use management IP pool")
            kubernetes_service = {
                "apiVersion": "v1",
                "kind": "Service",
                "metadata": {
                    "annotations": {
                        "metallb.universe.tf/address-pool": "ekpmgmt01",
                    },
                    "name": "kubernetes",
                    "namespace": "default",
                },
                "spec": {
                    "type": "LoadBalancer",
                },
            }
            file = tempfile.NamedTemporaryFile(mode="w+", delete=False)
            yaml.dump(kubernetes_service, file)
            file.close()

            service_cmd = ["apply", "-f", file.name]
            try:
                launch_kubectl_command(service_cmd)
            except ex as CommandException:
                Path(file.name).unlink()
                raise ex

            LOG.debug("service updated successfully")

        if "load_balancers_ips" in cfg["ekp"].keys():
            # Installing metallb definitions
            LOG.debug("Beginning metallb definitions installation")
            metallbDefinitions = [
                {
                    "apiVersion": "metallb.io/v1beta1",
                    "kind": "IPAddressPool",
                    "metadata": {
                        "name": "ekppool01",
                        "namespace": "metallb-system",
                    },
                    "spec": {
                        "addresses": cfg["ekp"]["load_balancers_ips"],
                    },
                },
                {
                    "apiVersion": "metallb.io/v1beta1",
                    "kind": "L2Advertisement",
                    "metadata": {
                        "name": "ekppool01",
                        "namespace": "metallb-system",
                    },
                    "spec": {
                        "ipAddressPools": [
                            "ekppool01",
                        ],
                    },
                },
            ]
            file = tempfile.NamedTemporaryFile(mode="w+", delete=False)
            yaml.dump_all(metallbDefinitions, file)
            file.close()

            metallb_cmd = [KUBECTL_BIN, "create", "-f", file.name]
            command = subprocess.run(
                metallb_cmd,
                shell=False,
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
            )
            Path(file.name).unlink()

            if command.returncode != 0:
                raise CommandException(
                    f"Got issue trying to install metallb definitions\n{command.stdout}"
                )
            LOG.debug("Definitions installed successfully")

        # Install Authentik via helm

        # Add authentik helm repo
        LOG.debug("Adding authentik helm repo")
        helm_cmd = ["repo", "add", "authentik", "https://charts.goauthentik.io"]
        launch_helm_command(helm_cmd)
        LOG.debug("Done")

        # Refresh helm repos
        LOG.debug("Refreshing helm repos")
        helm_cmd = ["repo", "update"]
        launch_helm_command(helm_cmd)
        LOG.debug("Done")

        # Generate authentik values.yml file
        LOG.debug("Installing authentik helm chart")
        authentik_secret_key = "".join(random.choices(string.printable, k=20))
        authentik_password = "".join(random.choices(string.printable, k=20))
        authentik_conf = {
            "authentik": {
                "secret_key": authentik_secret_key,
                "postgresql": {
                    "password": authentik_password,
                },
            },
            "server": {
                "ingress": {
                    "ingressClassName": "traefik",
                    "enabled": True,
                    "hosts": [
                        f"authentik.{cfg['ekp']['services_domain']}",
                    ],
                },
            },
            "postgresql": {
                "enabled": True,
                "auth": {
                    "password": authentik_password,
                },
            },
            "redis": {
                "enabled": True,
            },
        }
        file = tempfile.NamedTemporaryFile(mode="w+", delete=False)
        yaml.dump(authentik_conf, file)
        file.close()

        # Install Authentik helm chart
        helm_cmd = [
            "upgrade",
            "--install",
            "authentik",
            "-n",
            "authentik",
            "--create-namespace",
            "authentik/authentik",
            "-f",
            file.name,
        ]
        try:
            launch_helm_command(helm_cmd)
        except ex as CommandException:
            Path(file.name).unlink()
            raise ex
        LOG.debug("Done")

    LOG.debug(f"END module {name}")
